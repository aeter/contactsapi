import pytest
import json

import contactsapi.app     # inits routes for testing
import contactsapi.models  # inits db for testing


@pytest.fixture
def client():
    contactsapi.models.db.create_all()  # setup db for each test

    client = contactsapi.APP.test_client()
    yield client

    contactsapi.models.db.drop_all()  # cleanup db for each test


def test_empty_db(client):
    rv = client.get('/v1/contacts/')
    assert json.loads(rv.data) == []


def test_create_contact(client):
    json_data = {
        'username': 'sirsir',
        'first_name': 'john',
        'last_name': 'silver',
        'emails': ['john@example.com'],
    }
    rv = client.post('/v1/contacts/', json=json_data, follow_redirects=True)
    rv = json.loads(rv.data)
    assert rv.get('username', '') == 'sirsir'
    assert rv.get('id', '') != ''


def test_delete_contact_and_related_emails(client):
    json_data = {'username': 't', 'first_name': 't', 'last_name': 't',
                 'emails': ['t@example.com']}
    rv = client.post('/v1/contacts/', json=json_data, follow_redirects=True)
    id = json.loads(rv.data)['id']
    assert contactsapi.models.Contact.query.count() == 1
    assert contactsapi.models.Email.query.count() == 1

    rv = client.delete(f"/v1/contacts/{id}")
    count_deleted = json.loads(rv.data)
    assert count_deleted == 1
    assert contactsapi.models.Contact.query.count() == 0
    assert contactsapi.models.Email.query.count() == 0


def test_find_contact_by_username(client):
    username = 'x'
    json_data = {'username': username, 'first_name': 'xx', 'last_name': 'xx',
                 'emails': ['xx@example.com']}
    rv = client.post('/v1/contacts/', json=json_data, follow_redirects=True)
    assert contactsapi.models.Contact.query.count() == 1

    rv = client.get(f"/v1/contacts/{username}")
    rv = json.loads(rv.data)
    assert rv.get('username', '') == username
    assert rv.get('emails', []) == ['xx@example.com']


def test_update_contact(client):
    json_data = {'username': 'a', 'first_name': 'aa', 'last_name': 'aa',
                 'emails': ['aa@example.com']}
    rv = client.post('/v1/contacts/', json=json_data, follow_redirects=True)
    id = json.loads(rv.data)['id']
    assert contactsapi.models.Contact.query.count() == 1

    json_data = {'emails': ['t@example.com']}
    rv = client.patch(f"/v1/contacts/{id}", json=json_data)
    assert contactsapi.models.Contact.query.first().to_dict()['emails'] == \
        ["t@example.com"]
