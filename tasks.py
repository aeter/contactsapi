import datetime
import random
import string

from celery import Celery

from contactsapi.models import Contact


REDIS = 'redis://localhost:6379'
celery = Celery(__name__, backend=REDIS, broker=REDIS)
celery.conf.beat_schedule = {
    'create-random-contact-every-n-seconds': {
        'task': 'tasks.create_random_contact',
        'schedule': 15.0,
    },
    'delete-older-contacts': {
        'task': 'tasks.delete_older_contacts',
        'schedule': 1.0,
    },
}
celery.conf.enable_utc = True


def random_string(length):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(length))


@celery.task(name='tasks.create_random_contact')
def create_random_contact():
    random_data = {
        'username': random_string(6),
        'first_name': random_string(8),
        'last_name': random_string(8),
        'emails': [
            f"{random_string(5)}@example.com",
            f"{random_string(10)}@example.com",
        ],
    }
    response = Contact.create_with(**random_data)
    print(response)


@celery.task(name='tasks.delete_older_contacts')
def delete_older_contacts():
    current_time = datetime.datetime.utcnow()
    one_minute_ago = current_time - datetime.timedelta(seconds=60)
    for c in Contact.query.filter(Contact.created_at < one_minute_ago).all():
        Contact.delete_with(id=c.id)
