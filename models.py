import os
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import exc
from sqlalchemy.orm import joinedload

from contactsapi import APP

if os.environ.get('TEST', '') == 'runtests':
    APP.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///:memory:"
else:
    APP.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///contactsapi.db'
db = SQLAlchemy(APP)
db.init_app(APP)


class Contact(db.Model):
    __tablename__ = 'contacts'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    first_name = db.Column(db.String(80), nullable=False)
    last_name = db.Column(db.String(80), nullable=False)
    created_at = db.Column(db.DateTime, server_default=db.func.now())
    emails = db.relationship('Email', backref='contacts',
                             cascade='all,delete,delete-orphan', lazy=True)

    @staticmethod
    def all():
        return Contact.query.options(joinedload(Contact.emails)).all()

    @staticmethod
    def find_by_username(username):
        return Contact.query.options(joinedload(Contact.emails)).filter_by(
            username=username).first()

    @staticmethod
    def create_with(**kwargs):
        try:
            emails = kwargs.get('emails', [])
            if 'emails' in kwargs:
                del kwargs['emails']

            contact = Contact(**kwargs)
            db.session.add(contact)
            db.session.commit()

            for e in emails:
                email = Email(contact_id=contact.id, email=e)
                db.session.add(email)
            db.session.commit()

            return contact.to_dict()
        except exc.IntegrityError as e:
            print(e)
            return {'error': "Email or username already used"}

    @staticmethod
    def delete_with(**kwargs):
        count_deleted = 0
        contact = db.session.query(Contact).filter_by(**kwargs).first()
        if contact is not None:
            db.session.delete(contact)
            db.session.commit()
            count_deleted = 1
        return count_deleted

    @staticmethod
    def update_with(**kwargs):
        try:
            if 'emails' in kwargs:
                # recreate the emails for the contact in the DB with new values
                emails = kwargs['emails']
                del kwargs['emails']
                Email.query.filter_by(contact_id=kwargs["id"]).delete()
                db.session.commit()
                for e in emails:
                    email = Email(contact_id=kwargs["id"], email=e)
                    db.session.add(email)
                db.session.commit()

            db.session.query(Contact).filter_by(
                    id=kwargs["id"]).update(kwargs)
            db.session.commit()
            return Contact.query.filter_by(id=kwargs["id"]).first().to_dict()
        except exc.IntegrityError as e:
            print(e)
            return {'error': "Email or username already used"}

    def __repr__(self):
        return '<Contact %r>' % self.username

    def to_dict(self):
        attrs = {c.name: getattr(self, c.name) for c in self.__table__.columns}
        attrs['emails'] = [e.email for e in self.emails]
        return attrs


class Email(db.Model):
    __tablename__ = 'emails'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(50), unique=True, nullable=False)
    contact_id = db.Column(db.Integer, db.ForeignKey('contacts.id'),
                           nullable=False)


# this line should be after all models.
db.create_all()
