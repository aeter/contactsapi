from flask import jsonify, request

from contactsapi import APP
from contactsapi.models import Contact


@APP.route('/v1/contacts/', methods=['GET'])
def list_contacts():
    return jsonify([contact.to_dict() for contact in Contact.all()])


@APP.route('/v1/contacts/', methods=['POST'])
def create_contact():
    return jsonify(Contact.create_with(**request.get_json()))


@APP.route('/v1/contacts/<contact_id>', methods=['DELETE'])
def delete_contact(contact_id):
    return jsonify(Contact.delete_with(id=contact_id))


@APP.route('/v1/contacts/<contact_id>', methods=['PATCH'])
def update_contact(contact_id):
    kwargs = request.get_json()
    kwargs["id"] = contact_id  # to disallow overriding `id` column by the API
    return jsonify(Contact.update_with(**kwargs))


@APP.route('/v1/contacts/<username>', methods=['GET'])
def contact_by_username(username):
    contact = Contact.find_by_username(username)
    if contact is None:
        return jsonify({})
    else:
        return jsonify(contact.to_dict())
