# README #

A minimal contacts API. Tested on Ubuntu 18.04.

## Prerequisites

```
sudo apt-get install python3 python3-venv make redis-server
```

## Running the server

```
make server
```

or (with extra debug info):

```
FLASK_ENV=development make server
```

## Running the celery background tasks
```
make celery
```

## Running tests

```
make test
```

## Adding a new python lib to requirements.txt
```
. venv/bin/activate
pip install <my_lib>
make freezedeps
```

