import os

from flask import Flask

APP = Flask(__name__)

APP.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False  # disable notifications

if os.environ.get('FLASK_ENV', None) == "development":
    APP.config['SQLALCHEMY_ECHO'] = True  # logs SQL queries
