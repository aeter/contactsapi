SHELL := /bin/bash

.SILENT: test
.PHONY: freezedeps server celery clean test

freezedeps:
	test -d venv || python3 -m venv venv ; \
	source venv/bin/activate; \
	pip freeze > requirements.txt; \

server:
	test -d venv || python3 -m venv venv ; \
	source venv/bin/activate; \
	pip install -r requirements.txt; \
	FLASK_APP=app.py flask run; \

celery:
	test -d venv || python3 -m venv venv ; \
	source venv/bin/activate; \
	pip install -r requirements.txt; \
	PYTHONPATH="../$(pwd)" celery worker --app contactsapi.tasks.celery --loglevel=INFO -B; \

clean:
	rm -rf venv; \
	find -iname "*.pyc" -delete; \
	find -iname "__pycache__" -delete; \

test:
	test -d venv || python3 -m venv venv ; \
	source venv/bin/activate; \
	pip install --quiet -r requirements.txt; \
	find -iname "*.py" -not -path "./venv/*" -exec pycodestyle "{}" \; ; \
	find -iname "*.py" -not -path "./venv/*" -exec pyflakes "{}" \; ; \
	TEST=runtests PYTHONPATH=.. pytest ; \
